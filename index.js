import { AppRegistry } from 'react-native';
// import App from './src/App';
import { DeviceConnectWizzard } from './src/apps/DeviceConnectWizzard';
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => DeviceConnectWizzard);
