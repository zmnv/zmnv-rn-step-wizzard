import React, { PureComponent } from 'react';
// import PropTypes from 'prop-types';

import { Steps } from '~/theme/components';

import { SearchingStep, OptionSelectStep, FinishStep } from './components';

class DeviceConnectWizzard extends PureComponent {
    wizzardRef = React.createRef();

    steps = [
        SearchingStep.create(this.wizzardRef),
        OptionSelectStep.create(this.wizzardRef),
        {
            title: 'Device\nupdate',
            iconName: 'bluetooth',
            description: 'Downloading update for the device',
        },
        // {
        //     title: 'Extension\nupdate',
        //     iconName: 'bluetooth',
        // },
        FinishStep.create(this.wizzardRef),
    ];

    render() {
        return (
            <Steps.Wizzard ref={this.wizzardRef} steps={this.steps} />
        );
    }
}

DeviceConnectWizzard.propTypes = {
};

DeviceConnectWizzard.defaultProps = {
};

export default DeviceConnectWizzard;
