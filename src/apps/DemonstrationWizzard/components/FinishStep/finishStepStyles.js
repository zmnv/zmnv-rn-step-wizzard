import { StyleSheet } from 'react-native';
import { layout } from '~/theme/styles';

export const finishStepStyles = StyleSheet.create({
    root: {
        ...layout.marginHorizontalNormal,
        ...layout.marginBottomNormal,
        // backgroundColor: 'rgba(255, 0, 0, 0.1)',
        borderRadius: 4,
        flexGrow: 1,
        justifyContent: 'flex-end',
    },
    buttonOffset: layout.marginTopNormal,
});
