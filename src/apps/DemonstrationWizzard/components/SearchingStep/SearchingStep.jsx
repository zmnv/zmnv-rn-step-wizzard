import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View, Text } from 'react-native';

import { Button } from '~/theme/components';
import { StepModel, getWizzardRef, StepModelProps, StepExample } from '~/theme/components/Steps/models';

import { searchingStepStyles as styles } from './searchingStepStyles';
import { textColors } from '~/theme/styles';

let prikol = 0;
function testWizzardSteps(wizzard) {
    wizzard.insertStepsAfterSlug([
        StepExample(`${prikol} 1\n`),
        StepExample(`${prikol} 2\n`),
        StepExample(`${prikol} 3\n`),
    ],
    'STEPS_OPTION_SELECT', () => {
        wizzard.insertStepsAndNext([
            StepExample(`${prikol} 4\n`),
        ]);
        prikol += 1;
    });
}

class SearchingStep extends PureComponent {
    static propTypes = {
        wizzard: PropTypes.any.isRequired,
        ...StepModelProps.propTypes,
    };

    static defaulProps = StepModelProps.defaultProps;

    handleClick = () => {
        const wizzard = getWizzardRef(this.props.wizzard);

        if (wizzard) {
            testWizzardSteps(wizzard);
        }
    }

    render() {
        return (
            <View style={styles.root}>
                <Button title="Добавить шаг изнутри" onPress={this.handleClick} />
            </View>
        );
    }
}

SearchingStep.create = wizzard => StepModel.create({
    slug: 'STEPS_SEARCHING',
    title: 'Searching',
    iconName: 'search',
    description: (
        <Fragment>
            Please check that you installed the sensor according
            <Text style={textColors.textLinkColor}> to the manual</Text>
        </Fragment>
    ),
    children: <SearchingStep wizzard={wizzard} />,
});

export default SearchingStep;
