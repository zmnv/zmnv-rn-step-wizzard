import { StyleSheet } from 'react-native';

export const optionSelectStepStyles = StyleSheet.create({
    root: {
        marginHorizontal: 16,
        marginTop: 24,
        paddingTop: 4,
        backgroundColor: 'rgba(255, 0, 0, 0.1)',
        borderRadius: 4,
    },
});
