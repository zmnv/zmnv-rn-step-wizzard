import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View, Button } from 'react-native';

import { StepModel, getWizzardRef, StepModelProps } from '~/theme/components/Steps/models';

import { optionSelectStepStyles as styles } from './optionSelectStepStyles';

class OptionSelectStep extends PureComponent {
    static propTypes = {
        wizzard: PropTypes.any.isRequired,
        ...StepModelProps.propTypes,
    };

    static defaulProps = StepModelProps.defaultProps;

    handleClick = () => {
        const wizzard = getWizzardRef(this.props.wizzard);

        if (wizzard) {
            const keck = StepModel.create({
                title: `После\nOption\n${new Date().getTime()}`,
            });

            wizzard.insertStepsAndNext([keck]);
        }
    }

    render() {
        return (
            <View style={styles.root}>
                <Button title={this.props.title} onPress={this.handleClick} />
            </View>
        );
    }
}

OptionSelectStep.create = wizzard => StepModel.create({
    slug: 'STEPS_OPTION_SELECT',
    title: 'Option\nselect',
    iconName: 'memory',
    description: 'Please select "Device with extension" if you would like to see an additional step',
    children: <OptionSelectStep title="Прикол 222" wizzard={wizzard} />,
});

export default OptionSelectStep;
