import { StyleSheet } from 'react-native';
import { layout } from '~/theme/styles';

export const searchingStepStyles = StyleSheet.create({
    root: {
        marginHorizontal: 16,
        marginTop: 24,
        // paddingTop: 24,
        // backgroundColor: 'rgba(255, 0, 0, 0.1)',
        // borderRadius: 4,
        alignItems: 'center',
        // justifyContent: 'center',
        flexGrow: 1,
    },
    bottomOffset: layout.marginBottomBase,
});
