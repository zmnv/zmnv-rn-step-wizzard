import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View, Text, Image } from 'react-native';

import { FadePulsation } from '~/theme/components';
import { StepModel, getWizzardRef, StepModelProps } from '~/theme/components/Steps/models';

import { searchingStepStyles as styles } from './searchingStepStyles';
import { textColors } from '~/theme/styles';
import { device } from '~/theme/vars';

const WAIT_DURATION = 5000;
let startTimeout;

class SearchingStep extends PureComponent {
    static propTypes = {
        wizzard: PropTypes.any.isRequired,
        ...StepModelProps.propTypes,
    };

    static defaulProps = StepModelProps.defaultProps;

    componentDidMount() {
        this.stepExecution();
    }

    componentWillUnmount() {
        clearTimeout(startTimeout);
    }

    handleClick = () => {
        const wizzard = getWizzardRef(this.props.wizzard);

        if (wizzard) {
            wizzard.nextIndex();
        }
    }

    stepExecution() {
        startTimeout = setTimeout(
            this.handleClick,
            WAIT_DURATION,
        );
    }

    render() {
        console.log('height', device.height);
        return (
            <View style={styles.root}>
                {/*
                    Количество мерцаний = WAIT_DURATION / duration * 2
                */}
                <FadePulsation fromValue={0.05} duration={500}>
                    <Image source={require('./assets/searching-device/searching-device.png')} />
                </FadePulsation>
            </View>
        );
    }
}

SearchingStep.create = wizzard => StepModel.create({
    slug: 'STEPS_SEARCHING',
    title: 'Searching',
    iconName: 'search',
    description: (
        <Fragment>
            Please check that you installed the sensor according
            <Text style={textColors.textLinkColor}> to the manual</Text>
        </Fragment>
    ),
    children: <SearchingStep wizzard={wizzard} />,
});

export default SearchingStep;
