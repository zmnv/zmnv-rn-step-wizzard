import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View } from 'react-native';

import { Button } from '~/theme/components';
import { StepModel, StepModelProps, getWizzardRef } from '~/theme/components/Steps';

import { finishStepStyles as styles } from './finishStepStyles';

class FinishStep extends PureComponent {
    static propTypes = {
        wizzard: PropTypes.any.isRequired,
        ...StepModelProps.propTypes,
    };

    static defaulProps = StepModelProps.defaultProps;

    handleClick = () => {
        const wizzard = getWizzardRef(this.props.wizzard);
        if (wizzard) {
            wizzard.resetState();
        }
    }

    render() {
        return (
            <View style={styles.root}>
                <Button type="filled" title="Go to site dashboard" onPress={this.handleClick} />
                <View style={styles.buttonOffset} />
                <Button title="Add futher devices" onPress={this.handleClick} />
            </View>
        );
    }
}

FinishStep.create = wizzard => StepModel.create({
    slug: 'STEPS_FINISH',
    title: 'Finish',
    iconName: 'done',
    children: <FinishStep wizzard={wizzard} />,
    description: 'Device has been added',
    isFinal: true,
});

export default FinishStep;
