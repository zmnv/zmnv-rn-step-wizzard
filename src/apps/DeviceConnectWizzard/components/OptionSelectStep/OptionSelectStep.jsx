import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View, Button } from 'react-native';

import { Divider, ListItem, ListItemContainer } from '~/theme/components';
import { StepModel, getWizzardRef, StepModelProps, StepExample } from '~/theme/components/Steps/models';

import { optionSelectStepStyles as styles } from './optionSelectStepStyles';

class OptionSelectStep extends PureComponent {
    static propTypes = {
        wizzard: PropTypes.any.isRequired,
        ...StepModelProps.propTypes,
    };

    static defaulProps = StepModelProps.defaultProps;

    handleDevicePress = () => {
        /** TODO: можно выше */
        const wizzard = getWizzardRef(this.props.wizzard);
        if (wizzard) {
            wizzard.nextIndex();
        }
    }

    handleDeviceWithPress = () => {
        const wizzard = getWizzardRef(this.props.wizzard);
        if (wizzard) {
            wizzard.insertStepsAfterSlug([
                StepExample('With\n'),
            ], 'STEPS_DEVICE_UPDATE', wizzard.nextIndex);
        }
    }

    render() {
        return (
            <View style={styles.root}>
                <ListItemContainer>
                    <ListItem
                        title="Device"
                        iconName="wifi"
                        showNextIcon
                        onPress={this.handleDevicePress}
                    />
                    <Divider />
                    <ListItem
                        title="Device with extension"
                        iconName="wifi"
                        showNextIcon
                        onPress={this.handleDeviceWithPress}
                    />
                </ListItemContainer>
            </View>
        );
    }
}

OptionSelectStep.create = wizzard => StepModel.create({
    slug: 'STEPS_OPTION_SELECT',
    title: 'Option\nselect',
    iconName: 'memory',
    description: 'Please select "Device with extension" if you would like to see an additional step',
    children: <OptionSelectStep title="Прикол 222" wizzard={wizzard} />,
});

export default OptionSelectStep;
