import { StyleSheet } from 'react-native';
import { units } from '~/theme/vars';

export const optionSelectStepStyles = StyleSheet.create({
    root: {
        width: '100%',
        maxWidth: 480,
        paddingHorizontal: units.u12,
        // marginTop: 24,
        // backgroundColor: 'rgba(255, 0, 0, 0.1)',
        borderRadius: 4,
        alignSelf: 'center',
    },
});
