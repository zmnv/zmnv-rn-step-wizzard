// Основано на https://stackoverflow.com/a/28056903

/**
 * Преобразовать цвет:
 * 
 * `#FF00FF` в `rgba(255, 0, 255, 1)`
 * 
 * @param {string} hex цвет
 * @param {number} alpha прозрачность от 0 до 1
 */
export function hexToRGB(hex, alpha, source) {
    const r = parseInt(hex.slice(1, 3), 16);
    const g = parseInt(hex.slice(3, 5), 16);
    const b = parseInt(hex.slice(5, 7), 16);

    const colorString = `${r}, ${g}, ${b}`;

    if (source) {
        return colorString;
    }

    if (alpha) {
        return `rgba(${colorString}, ${alpha})`;
    }

    return `rgb(${colorString})`;
}
