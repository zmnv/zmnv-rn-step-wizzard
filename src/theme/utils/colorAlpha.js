import { hexToRGB } from './hexToRGB';

/**
 * Изменить прозрачность цвета
 *
 * @param {string} color цвет в формате RGBA или HEX
 * @param {number} alpha прозрачность от 0 до 1
 */
export function colorAlpha(color, alpha = 1) {
    if (!color) {
        if (__DEV__) {
            // eslint-disable-next-line no-console
            console.log('[Ошибка] Не указан цвет');
        }

        return null;
    }

    if (color[0] === '#') {
        return hexToRGB(color, alpha);
    }

    return color.replace(/[\d\\.]+\)$/g, `${alpha})`);
}
