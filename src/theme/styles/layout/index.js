import { StyleSheet } from 'react-native';
import { backgroundColors } from '~/theme/styles/colors';

import {
    paddingHorizontal,
    marginHorizontal,
} from './horizontal';

import {
    paddingVertical,
    marginVertical,
} from './vertical';

import {
    paddingLeft,
    marginLeft,
} from './left';

import {
    paddingRight,
    marginRight,
} from './right';

import {
    paddingTop,
    marginTop,
} from './top';

import {
    paddingBottom,
    marginBottom,
} from './bottom';

const flex = {
    layoutGrow: {
        flexGrow: 1,
    },
    layoutFull: {
        flex: 1,
    },
};

const screen = {
    ...flex.layoutFull,
    ...backgroundColors.backgroundPrimaryColor,
};

export const layout = StyleSheet.create({
    ...marginHorizontal,
    ...paddingHorizontal,
    ...paddingVertical,
    ...marginVertical,

    ...paddingLeft,
    ...marginLeft,
    ...paddingRight,
    ...marginRight,

    ...paddingTop,
    ...marginTop,
    ...paddingBottom,
    ...marginBottom,
    ...flex,
    screen,
});
