import { units } from '~/theme/vars';

export const paddingBottom = {
    paddingBottomNull: {
        paddingBottom: units.u0,
    },
    paddingBottomSmall: {
        paddingBottom: units.u4,
    },
    paddingBottomBase: {
        paddingBottom: units.u8,
    },
    paddingBottomNormal: {
        paddingBottom: units.u16,
    },
    paddingBottomMedium: {
        paddingBottom: units.u24,
    },
};

export const marginBottom = {
    marginBottomNull: {
        marginBottom: units.u0,
    },
    marginBottomSmall: {
        marginBottom: units.u4,
    },
    marginBottomBase: {
        marginBottom: units.u8,
    },
    marginBottomNormal: {
        marginBottom: units.u16,
    },
    marginBottomMedium: {
        marginBottom: units.u24,
    },
};
