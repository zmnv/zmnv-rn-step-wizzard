import { units } from '~/theme/vars';

export const paddingTop = {
    paddingTopNull: {
        paddingTop: units.u0,
    },
    paddingTopSmall: {
        paddingTop: units.u4,
    },
    paddingTopBase: {
        paddingTop: units.u8,
    },
    paddingTopNormal: {
        paddingTop: units.u16,
    },
    paddingTopMedium: {
        paddingTop: units.u24,
    },
};

export const marginTop = {
    marginTopNull: {
        marginTop: units.u0,
    },
    marginTopSmall: {
        marginTop: units.u4,
    },
    marginTopBase: {
        marginTop: units.u8,
    },
    marginTopNormal: {
        marginTop: units.u16,
    },
    marginTopMedium: {
        marginTop: units.u24,
    },
};
