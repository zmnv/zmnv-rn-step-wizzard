import { units } from '~/theme/vars';

export const paddingVertical = {
    paddingVerticalNull: {
        paddingVertical: units.u0,
    },
    paddingVerticalSmall: {
        paddingVertical: units.u4,
    },
    paddingVerticalBase: {
        paddingVertical: units.u8,
    },
    paddingVerticalNormal: {
        paddingVertical: units.u16,
    },
    paddingVerticalMedium: {
        paddingVertical: units.u24,
    },
};

export const marginVertical = {
    marginVerticalNull: {
        marginVertical: units.u0,
    },
    marginVerticalSmall: {
        marginVertical: units.u4,
    },
    marginVerticalBase: {
        marginVertical: units.u8,
    },
    marginVerticalNormal: {
        marginVertical: units.u16,
    },
    marginVerticalMedium: {
        marginVertical: units.u24,
    },
};
