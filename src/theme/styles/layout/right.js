import { units } from '~/theme/vars';

export const paddingRight = {
    paddingRightNull: {
        paddingRight: units.u0,
    },
    paddingRightSmall: {
        paddingRight: units.u4,
    },
    paddingRightBase: {
        paddingRight: units.u8,
    },
    paddingRightNormal: {
        paddingRight: units.u16,
    },
    paddingRightMedium: {
        paddingRight: units.u24,
    },
};

export const marginRight = {
    marginRightNull: {
        marginRight: units.u0,
    },
    marginRightSmall: {
        marginRight: units.u4,
    },
    marginRightBase: {
        marginRight: units.u8,
    },
    marginRightNormal: {
        marginRight: units.u16,
    },
    marginRightMedium: {
        marginRight: units.u24,
    },
};
