import { units } from '~/theme/vars';

export const paddingLeft = {
    paddingLeftNull: {
        paddingLeft: units.u0,
    },
    paddingLeftSmall: {
        paddingLeft: units.u4,
    },
    paddingLeftBase: {
        paddingLeft: units.u8,
    },
    paddingLeftNormal: {
        paddingLeft: units.u16,
    },
    paddingLeftMedium: {
        paddingLeft: units.u24,
    },
};

export const marginLeft = {
    marginLeftNull: {
        marginLeft: units.u0,
    },
    marginLeftSmall: {
        marginLeft: units.u4,
    },
    marginLeftBase: {
        marginLeft: units.u8,
    },
    marginLeftNormal: {
        marginLeft: units.u16,
    },
    marginLeftMedium: {
        marginLeft: units.u24,
    },
};
