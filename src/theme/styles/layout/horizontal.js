import { units } from '~/theme/vars';

export const paddingHorizontal = {
    paddingHorizontalNull: {
        paddingHorizontal: units.u0,
    },
    paddingHorizontalSmall: {
        paddingHorizontal: units.u4,
    },
    paddingHorizontalBase: {
        paddingHorizontal: units.u8,
    },
    paddingHorizontalNormal: {
        paddingHorizontal: units.u16,
    },
    paddingHorizontalMedium: {
        paddingHorizontal: units.u24,
    },
};

export const marginHorizontal = {
    marginHorizontalNull: {
        marginHorizontal: units.u0,
    },
    marginHorizontalSmall: {
        marginHorizontal: units.u4,
    },
    marginHorizontalBase: {
        marginHorizontal: units.u8,
    },
    marginHorizontalNormal: {
        marginHorizontal: units.u16,
    },
    marginHorizontalMedium: {
        marginHorizontal: units.u24,
    },
};
