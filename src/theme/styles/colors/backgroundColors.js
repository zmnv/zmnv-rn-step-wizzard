import { StyleSheet } from 'react-native';
import { colors } from '~/theme/vars';

export const backgroundColors = StyleSheet.create({
    backgroundPrimaryColor: {
        backgroundColor: colors.surfaces.primary,
    },
    backgroundSecondaryColor: {
        backgroundColor: colors.surfaces.secondary,
    },
    backgroundLinkColor: {
        backgroundColor: colors.types.link,
    },
    backgroundSuccessColor: {
        backgroundColor: colors.types.success,
    },
    backgroundDangerColor: {
        backgroundColor: colors.types.danger,
    },
});
