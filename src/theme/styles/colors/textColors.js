import { StyleSheet } from 'react-native';
import { colors } from '~/theme/vars';

export const textColors = StyleSheet.create({
    textPrimaryColor: {
        color: colors.base.white,
    },
    textSecondaryColor: {
        color: colors.base.gray,
    },
    textLinkColor: {
        color: colors.types.link,
    },
    textSuccessColor: {
        color: colors.types.success,
    },
    textDangerColor: {
        color: colors.types.danger,
    },
});
