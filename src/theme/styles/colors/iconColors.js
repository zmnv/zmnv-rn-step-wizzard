import { StyleSheet } from 'react-native';
import { colors } from '~/theme/vars';

export const iconColors = StyleSheet.create({
    iconPrimaryColor: {
        color: colors.base.white,
    },
    iconSecondaryColor: {
        color: colors.base.grayAlt,
    },
    iconInactiveColor: {
        color: colors.base.grayInactive,
    },
});
