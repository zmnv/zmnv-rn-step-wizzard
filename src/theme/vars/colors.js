import { colorAlpha } from '~/theme/utils';

const OPACITY_DEFAULT = 0.3;

const base = {
    black: '#0D0E12',
    blackAlt: '#15161A',
    gray: '#99AAB0',
    grayAlt: '#B2BFC3',
    grayInactive: '#616B71',
    white: '#FFFFFF',
    divider: '#23262B',

    palette() {
        return ({
            ...this,
            grayOpacity: colorAlpha(this.gray, 0.1),
            whiteOpacity: colorAlpha(this.white, OPACITY_DEFAULT),
        });
    },
};

const brand = {
    primary: '#69c5b3',
    secondary: '#f0926c',

    palette() {
        return ({
            ...this,
            primaryOpacity: colorAlpha(this.primary, 0.1),
            secondaryOpacity: colorAlpha(this.secondary, OPACITY_DEFAULT),
        });
    },
};

const types = {
    link: '#0074FF',
    success: '#018E16',
    danger: '#FF3C00',

    palette() {
        return ({
            ...this,
            linkOpacity: colorAlpha(this.link, OPACITY_DEFAULT),
            successOpacity: colorAlpha(this.success, OPACITY_DEFAULT),
            dangerOpacity: colorAlpha(this.danger, OPACITY_DEFAULT),
        });
    },
};

const surfaces = {
    primary: base.black,
    secondary: base.blackAlt,
};

export const colors = {
    base: base.palette(),
    brand: brand.palette(),
    types: types.palette(),
    surfaces,
};
