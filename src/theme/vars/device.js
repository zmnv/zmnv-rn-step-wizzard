import { Dimensions } from 'react-native';

/**
 * Размеры экрана мобильного устройства
 */
export const device = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
};
