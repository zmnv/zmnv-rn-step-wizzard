/**
 * Набор настроек для «безопасной зоны»
 * 
 * ```jsx
 * <SafeAreaView forceInset={safeAreaInsets.onlyBottom} />
 * ```
 */
export const safeAreaInsets = {
    onlyTop: { top: 'always', bottom: 'never' },
    onlyBottom: { top: 'never', bottom: 'always' },
    alwaysVertical: { vertical: 'always' },
};
