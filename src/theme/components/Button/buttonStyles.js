import { StyleSheet } from 'react-native';

import { material } from 'react-native-typography';
import { colors, units } from '~/theme/vars';
import { layout, textColors } from '~/theme/styles';

export const loadingIndicatorColor = colors.base.white;

export const buttonStyles = StyleSheet.create({
    root: {
        ...layout.paddingHorizontalBase,
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 56,
        borderRadius: units.u4,
    },
    title: {
        ...material.body2WhiteObject,
    },
});

export const buttonVariantsStyles = StyleSheet.create({
    filled: {
        backgroundColor: '#1A1B1F',
    },
    text: {
        backgroundColor: 'transparent',
    },
});

export const titleVariantsStyles = StyleSheet.create({
    filled: {
        color: colors.base.grayAlt,
    },
    text: textColors.textLinkColor,
});
