import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View, StyleSheet } from 'react-native';
import { colors } from '~/theme/vars';

const styles = StyleSheet.create({
    root: {
        // width: '100%',
        height: null,
        borderBottomColor: colors.base.divider,
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
});

function getRootStyle(style) {
    if (style) {
        return {...styles.root, ...style};
    }

    return styles.root;
}

class Divider extends PureComponent {
    render() {
        return <View style={getRootStyle(this.props.style)} />;
    }
}

Divider.propTypes = {
    style: PropTypes.object,
};

Divider.defaultProps = {
    style: null,
};

export default Divider;
