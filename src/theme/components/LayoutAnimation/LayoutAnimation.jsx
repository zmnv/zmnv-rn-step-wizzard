import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

import { UIManager, LayoutAnimation as Layout } from 'react-native';

// eslint-disable-next-line no-unused-expressions
UIManager.setLayoutAnimationEnabledExperimental
        && UIManager.setLayoutAnimationEnabledExperimental(true);

const DEFAULT_OPTIONS = (duration = 250, damping = 0.7) => ({
    duration,
    create: {
        type: Layout.Types.spring,
        property: 'scaleXY',
        springDamping: damping,
    },
    update: {
        type: Layout.Types.spring,
        property: 'opacity',
        springDamping: damping,
    },
    delete: {
        type: Layout.Types.spring,
        property: 'opacity',
        springDamping: damping,
    },
});

/**
 * Компонент для «ручного» вызова нативной анимации
 * с помощью вызова `setAnimation` через `ref`.
 *
 * Используйте вызов анимации **только перед** `this.setState()`.
 * 
 * TODO: доделать
 */
class LayoutAnimation extends PureComponent {
    static animate = options => {
        if (options) {
            Layout.configureNext(options || DEFAULT_OPTIONS());
        }

        Layout.easeInEaseOut();
    };

    setAnimation = () => {
        // const { duration, springDamping } = this.props;
        LayoutAnimation.animate(this.props.options);
    }

    render() {
        const { children } = this.props;

        if (children) {
            return (
                <Fragment>
                    {children}
                </Fragment>
            );
        }

        return null;
    }
}

LayoutAnimation.propTypes = {
    children: PropTypes.node.isRequired,
    /** Перезаписать параметры нативной анимации */
    options: PropTypes.object,
    /** Продолжительность появления/исчезновения в мс (не работает при `options`) */
    // duration: PropTypes.number,
    /** Коэффициент амортизации воспроизведения анимации (не работает при `options`) */
    // springDamping: PropTypes.number,
};

LayoutAnimation.defaultProps = {
    options: Layout.Presets.easeInEaseOut,
    // duration: 400,
    // springDamping: 0.7,
};

export default LayoutAnimation;
