import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';
import { layout } from '~/theme/styles';
import { units, colors } from '~/theme/vars';

// import { as styles } from './';

const styles = StyleSheet.create({
    root: {
        ...layout.paddingHorizontalNormal,
        // ...layout.paddingVerticalNormal,
        backgroundColor: colors.base.blackAlt,
        borderRadius: units.u4 + 1,
    },
});

class ListItemContainer extends PureComponent {
    render() {
        return (
            <View style={styles.root}>
                {this.props.children}
            </View>
        );
    }
}

ListItemContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

ListItemContainer.defaultProps = {
};

export default ListItemContainer;
