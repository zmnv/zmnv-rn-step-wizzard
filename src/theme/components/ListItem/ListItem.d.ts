import * as React from 'react';

export type ThemeColors = "black" | "gray" | "primary" | "accent" | "success" | "danger";

type numberOrString = Number | String;
type nodeOrFunction = Node | Function;

export interface ListItemClasses {
    root?: StyleSheetList;
    title?: StyleSheetList;
    icon?: StyleSheetList;
    nextIcon?: StyleSheetList;
    rightLabel?: StyleSheetList;
    highlightTitle?: StyleSheetList;
    higthlightRight?: StyleSheetList;
}

export interface ListItemMainProps {
    /** Текст */
    title: string;
    /** Цвет иконки и текста */
    color?: ThemeColors;
    /** Уменьшить размер текста */
    dense?: boolean;
    /** Название иконки */
    iconName?: string;
    /** Добавить разделитель */
    divider: boolean;
    /** Показать иконку «стрелка вправо» */
    showNextIcon: boolean;
    /** Текст в правой части */
    rightLabel: numberOrString;
    /** Отобразить компонент в правой части */
    rightView: nodeOrFunction;
    /** Подсветка: добавить цветной фон */
    highlight: boolean;
    /** Добавить цветную точку перед заголовком */
    highlightTitle: boolean;
    /** Добавить цветную точку справа */
    higthlightRight: boolean;
    /**
     * Композиция стилей
     */
    classes?: ListItemClasses;
}

export interface ListItemProps extends ListItemMainProps {
    /** Действие при нажатии на кликабельный элемент. */
    onPress?: (...args: any[]) => ListItemMainProps;
}

/**
 * Компонент кликабельного элемента списка с иконкой слева (на выбор)
 *
 * Используется в контекстом меню, а так же в обычных экранах
 * в качестве базового кликабельного элемента списка.
 * 
 * Используйте `ListItemSection` для отображения списка в секции.
 */
class ListItem extends React.PureComponent<ListItemProps, any> {
    /** Прокинуть событие нажатия на элемент */
    handlePress = () => {}

    /** Контейнер с иконкой слева */
    iconContainerView(): JSX.Element;
    /** Компонент разделителя */
    dividerView(): JSX.Element;
    /** Компонент области справа */
    rightView(): JSX.Element;
    /** Компонент для отображения «новизны» через круг */
    circleView(): JSX.Element;
    /** Компонент с главным текстом */
    titleView(): JSX.Element;

    render(): JSX.Element;
}

export default ListItem;
