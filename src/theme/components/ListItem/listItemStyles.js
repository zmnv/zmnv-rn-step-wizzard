import { StyleSheet } from 'react-native';
import { material } from 'react-native-typography';

import { units, colors } from '~/theme/vars';
import { iconColors } from '~/theme/styles';

export const listItemStyles = StyleSheet.create({
    listItemContainer: {
        flexDirection: 'row',
        // paddingLeft: units.u16,
        // paddingRight: units.u12,
        alignItems: 'center',
        height: units.u56,
    },
    iconContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        // width: 32,
        // backgroundColor: 'blue',
        marginRight: units.u8,
        opacity: 0.8,
    },
    listItemTitle: {
        flexGrow: 1,
        flexBasis: 1,
        ...material.body2WhiteObject,
    },
    listItemTitleDense: {
        ...material.body1WhiteObject,
    },
    rightContainer: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    rightIcon: {
        ...iconColors.iconInactiveColor,
        marginLeft: -2,
        color: colors.base.gray,
    },
    rightLabel: {
        ...material.captionWhiteObject,
        maxWidth: 128,
        marginLeft: units.u4,
        color: colors.base.gray,
        alignSelf: 'center',
    },
    rightLabelDense: {
    },
    dividerOffset: {
        marginLeft: units.u16,
    },
    dividerOffsetIcon: {
        marginLeft: units.u56,
    },
});
