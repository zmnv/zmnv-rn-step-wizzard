import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

import { TouchableOpacity, View, Text } from 'react-native';

import { Divider } from '~/theme/components/Divider';
import { Icon } from '~/theme/components/Icon';

import { textColors } from '~/theme/styles';
import { isFunction } from '~/theme/utils';

import { listItemStyles as styles } from './listItemStyles';

/**
 * Компонент кликабельного элемента списка с иконкой слева (на выбор)
 *
 * Используется в контекстом меню, а так же в обычных экранах
 * в качестве базового кликабельного элемента списка.
 * 
 * Используйте `ListItemSection` для отображения списка в секции.
 */
class ListItem extends PureComponent {
    /** Прокинуть событие нажатия на элемент */
    handlePress = () => {
        const { onPress, ...otherProps } = this.props;
        if (isFunction(onPress)) onPress(otherProps);
    }

    /** Контейнер с иконкой слева */
    iconContainerView() {
        const { iconName, color, classes } = this.props;
        const style = {
            ...(textColors[color] || textColors.textSecondaryColor),
            ...classes.icon,
        };

        return iconName && (
            <View style={styles.iconContainer}>
                <Icon size={24} name={iconName} style={style} />
            </View>
        );
    }

    /** Компонент разделителя */
    dividerView() {
        const { divider, iconName } = this.props;
        const dividerStyle = iconName
            ? styles.dividerOffsetIcon
            : styles.dividerOffset;

        return divider && (
            <Divider style={dividerStyle} />
        );
    }

    /** Компонент области справа */
    rightView() {
        const { showNextIcon, rightLabel, rightView, dense, classes } = this.props;

        const rightLabelStyle = [
            styles.rightLabel,
            dense && styles.rightLabelDense,
            classes.rightLabel,
        ];

        return (showNextIcon || rightLabel || rightView) && (
            <View style={styles.rightContainer}>
                {!!rightLabel && (
                    <Text style={rightLabelStyle} numberOfLines={1} ellipsizeMode="tail">{rightLabel}</Text>
                )}
                {isFunction(rightView) ? rightView() : rightView}
                {showNextIcon && (
                    <Icon
                        name="navigate-next"
                        size={24}
                        style={styles.rightIcon}
                    />
                )}
            </View>
        );
    }

    /** Компонент с главным текстом */
    titleView() {
        const { title, color, dense, classes } = this.props;
        const style = [
            styles.listItemTitle,
            dense && styles.listItemTitleDense,
            textColors[color] || textColors.black,
            classes.title,
        ];

        return (
            <Fragment>
                <Text style={style} numberOfLines={1} ellipsizeMode="tail">
                    {title}
                </Text>
            </Fragment>
        );
    }

    render() {
        const rootStyle = [
            styles.listItemContainer,
            this.props.highlight
                && styles.listItemContainerHighlight,
        ];

        return (
            <Fragment>
                <TouchableOpacity onPress={this.handlePress}>
                    <View style={rootStyle}>
                        {this.iconContainerView()}
                        {this.titleView()}
                        {this.props.higthlightRight && this.circleView()}
                        {this.rightView()}
                    </View>
                </TouchableOpacity>
                {this.dividerView()}
            </Fragment>
        );
    }
}

ListItem.propTypes = {
    /** Текст */
    title: PropTypes.string.isRequired,
    /** Цвет иконки и текста */
    color: PropTypes.oneOf([
        'textPrimaryColor',
        'textSecondaryColor',
        'textLinkColor',
        'textSuccessColor',
        'textDangerColor',
    ]),
    /** Уменьшить размер текста */
    dense: PropTypes.bool,
    /** Название иконки */
    iconName: PropTypes.string,
    /** Добавить разделитель */
    divider: PropTypes.bool,
    /** Показать иконку «стрелка вправо» */
    showNextIcon: PropTypes.bool,
    /** Текст в правой части */
    rightLabel: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    /** Отобразить компонент в правой части */
    rightView: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
    /** Подсветка: добавить цветной фон */
    highlight: PropTypes.bool,
    /** Добавить цветную точку перед заголовком */
    highlightTitle: PropTypes.bool,
    /** Добавить цветную точку справа */
    higthlightRight: PropTypes.bool,
    /** Композиция стилей */
    classes: PropTypes.shape({
        root: PropTypes.object,
        title: PropTypes.object,
        icon: PropTypes.object,
        nextIcon: PropTypes.object,
        rightLabel: PropTypes.object,
        highlightTitle: PropTypes.object,
        higthlightRight: PropTypes.object,
    }),
    /** Действие при нажатии на кликабельный элемент. */
    onPress: PropTypes.func,
};

ListItem.defaultProps = {
    iconName: null,
    color: null,
    dense: false,
    divider: false,
    showNextIcon: false,
    rightLabel: null,
    rightView: null,
    highlight: null,
    highlightTitle: null,
    higthlightRight: null,
    classes: {
        root: null,
        title: null,
        icon: null,
        nextIcon: null,
        rightLabel: null,
        highlightTitle: null,
        higthlightRight: null,
    },
    onPress: () => {},
};

export default ListItem;
