import * as React from 'react';

export interface IconProps {
    /** Название иконки */
    name?: string;
    /** Размер иконки (24 по умолчанию) */
    size?: number;
    /** Стиль иконки */
    style?: StyleSheetList;
}

/**
 * Базовый компонент иконки.
 */
declare const Icon: React.FC<IconProps>;

export default Icon;
