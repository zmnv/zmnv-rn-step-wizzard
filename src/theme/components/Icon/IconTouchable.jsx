import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { TouchableOpacity, StyleSheet } from 'react-native';

import Icon from './Icon';

const styles = StyleSheet.create({
    containerStyle: {
        width: 48,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

function getContainerStyle(styleProp, containerSize) {
    if (styleProp || containerSize) {
        const size = containerSize && {
            width: containerSize,
            height: containerSize,
        };

        return {
            ...styles.containerStyle,
            ...size,
            ...styleProp,
        };
    }

    return styles.containerStyle;
}

class IconTouchable extends PureComponent {
    iconView() {
        const { iconStyle, name, size } = this.props;
        return <Icon name={name} size={size} style={iconStyle} />;
    }

    render() {
        const {
            onPress,
            containerStyle,
            containerSize,
            disabled,
        } = this.props;

        return (
            <TouchableOpacity
                onPress={onPress}
                style={getContainerStyle(containerStyle, containerSize)}
                disabled={disabled}
            >
                {this.iconView()}
            </TouchableOpacity>
        );
    }
}

IconTouchable.propTypes = {
    name: PropTypes.string,
    size: PropTypes.number,
    disabled: PropTypes.bool,
    containerStyle: PropTypes.object,
    containerSize: PropTypes.number,
    iconStyle: PropTypes.object,
    onPress: PropTypes.func,
};

IconTouchable.defaultProps = {
    name: 'done',
    size: 24,
    disabled: false,
    containerStyle: null,
    containerSize: null,
    iconStyle: null,
    onPress: () => {},
};

export default IconTouchable;
