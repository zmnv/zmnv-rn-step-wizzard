import React from 'react';
import PropTypes from 'prop-types';

import { StyleSheet } from 'react-native';

import IconModule from 'react-native-vector-icons/MaterialIcons';
import { iconColors } from '~/theme/styles';

const styles = StyleSheet.create({
    icon: iconColors.iconSecondaryColor,
});

function getIconStyle(styleProp) {
    if (styleProp) {
        return {...styles.icon, ...styleProp};
    }

    return styles.icon;
}

const Icon = props => (
    <IconModule
        {...props}
        style={getIconStyle(props.style)}
    />
);

Icon.propTypes = {
    name: PropTypes.string,
    size: PropTypes.number,
    style: PropTypes.object,
};

Icon.defaultProps = {
    name: 'done',
    size: 24,
    style: {},
};

export default Icon;
