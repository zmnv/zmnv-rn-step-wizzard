import * as React from 'react';

export interface IconTouchableProps {
    /** Название иконки */
    name?: string;
    /** Размер иконки (24 по умолчанию) */
    size?: number;
    /** Отключить «нажатие» */
    disabled?: boolean;
    /** Стиль контейнера `TouchableOpacity` */
    containerStyle?: StyleSheetList;
    /** Размер контейнера */
    containerSize?: number;
    /** Стиль иконки `Icon` */
    iconStyle?: StyleSheetList;

    onPress?: (...args: any[]) => any;
}

/** Интерактивная иконка с контейнером */
const IconTouchable: React.FC<IconTouchableProps>;

export default IconTouchable;
