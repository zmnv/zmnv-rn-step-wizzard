import * as React from 'react';

export interface StepModelInterface {
    /**
     * Уникальный строковый идентификатор (slug)
     */
    slug: string;
    /** 
     * Название шага
     */
    title?: string;
    /** 
     * Название иконки
     */
    iconName?: string;
    /** 
     * Текст описания
     */
    description?: string | React.ReactNode;
    /**
     * Целевой компонент для шага
     */
    children?: React.ReactNode;
    /**
     * Этот элемент активен в списке шагов?
     */
    isActive?: boolean | undefined;
    /**
     * Это последний элемент в списке шагов?
     */
    isFinal?: boolean;
}
