import PropTypes from 'prop-types';

export const StepModelProps = {
    propTypes: {
        /**
         * Уникальный строковый идентификатор (slug)
         */
        slug: PropTypes.string,
        /** 
         * Название шага
         */
        title: PropTypes.string,
        /** 
         * Название иконки
         */
        iconName: PropTypes.string,
        /** 
         * Текст описания
         */
        description: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.node,
        ]),
        /**
         * Целевой компонент для шага
         */
        children: PropTypes.node,
        /**
         * Этот элемент активен в списке шагов?
         */
        isActive: PropTypes.bool,
        /**
         * Это последний элемент в списке шагов?
         */
        isFinal: PropTypes.bool,
    },
    defaultProps: {
        slug: null,
        title: 'Заголовок',
        iconName: 'info',
        description: null,
        children: null,
        isActive: false,
        isFinal: false,
    },
};

/**
 * Модель конфигурации одного шага
 * + StepModelInterface .d.ts
 */
export class StepModel {
    constructor(raw) {
        this.slug = raw.slug;
        this.title = raw.title || StepModelProps.defaultProps.title;
        this.iconName = raw.iconName || StepModelProps.defaultProps.iconName;
        this.description = raw.description || StepModelProps.defaultProps.description;
        this.children = raw.children || StepModelProps.defaultProps.children;
        this.isFinal = raw.isFinal;

        if (typeof raw.isActive === 'boolean') {
            this.isActive = raw.isActive;
        }
    }

    static create(raw) {
        return raw ? new StepModel(raw) : {};
    }

    static fromArray(raw = []) {
        return raw.map(n => StepModel.create(n));
    }
}
