import { randomEmoji, randomIcon } from '~/theme/utils';

import { StepModel } from '~/theme/components/Steps/models';

export const StepExample = prefix => StepModel.create({
    slug: `${new Date().getTime()}`,
    title: `${prefix || ''}${randomEmoji(3)}`,
    description: `${randomEmoji(5)}\n${randomEmoji(5)}\n${randomEmoji(5)}\n${randomEmoji(5)}\n${randomEmoji(5)}\n${randomEmoji(5)}\n${randomEmoji(5)}\n`,
    iconName: randomIcon(),
});
