import { StyleSheet } from 'react-native';

/** TODO */
export const stepsWizzardStyles = StyleSheet.create({
    manuaControl: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 48,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
    },
});
