import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View, Button } from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';

import { logWarning } from '~/theme/utils';
import { layout } from '~/theme/styles';
import { safeAreaInsets, device } from '~/theme/vars';

import { HeaderBar, LayoutAnimation } from '~/theme/components';
import { StepsDisplay } from '~/theme/components/Steps/StepsDisplay';
import { StepsList } from '~/theme/components/Steps/StepsList';

import { StepExample, StepModelProps } from '~/theme/components/Steps/models';

import { stepsWizzardStyles as styles } from './stepsWizzardStyles';

const isFunction = func => typeof func === 'function' ? func : null;

/**
 * Компонент пошагового волшебника
 * 
 * * хранит в себе копию оригинального списка шагов
 * * управляет сменой шагов (предыдущий, следующий)
 * * определяет индекс текущего шага
 */
class StepsWizzard extends PureComponent {
    initialState = {
        index: 0,
        steps: this.props.steps,
    }

    state = this.initialState;

    /** Сбросить конфигурацию шагов волшебника до начального состояния */
    resetState = () => {
        this.setState(this.initialState);
    }

    /** Перейти на следующий шаг */
    nextIndex = callback => {
        this.setState(({index}) => ({ index: index + 1 }), isFunction(callback));
    }

    /** Перейти на предыдущий шаг */
    prevIndex = callback => {
        this.setState(({index}) => ({ index: index - 1 }), isFunction(callback));
    }

    /** Вставить шаги — на вход принимает массив из объектов */
    insertSteps = (input = [], callback) => {
        LayoutAnimation.animate();

        this.setState(({index, steps}) => {
            const newSteps = [
                ...steps.slice(0, index + 1),
                ...input,
                ...steps.slice(index + 1, steps.length),
            ];

            return {
                steps: newSteps,
            };
        }, isFunction(callback));
    }

    /** Вставить шаги — на вход принимает массив из объектов */
    insertStepsAfterSlug = (input = [], slug, callback) => {
        if (slug) {
            const { steps = [] } = this.state;
            const foundStepId = steps.findIndex(item => item.slug === slug);

            if (foundStepId) {
                LayoutAnimation.animate();

                this.setState({
                    steps: [
                        ...steps.slice(0, foundStepId + 1),
                        ...input,
                        ...steps.slice(foundStepId + 1, steps.length),
                    ],
                }, isFunction(callback));
            } else {
                logWarning(`Шаг с параметром slug = ${slug} не найден в списке шагов`, steps);
            }
        } else {
            logWarning('Не задан <slug>');
        }
    }

    /** 
     * Последовательность:
     * 1. Вставить шаги — на вход принимает массив из объектов
     * 2. Перейти на следующий шаг
     */
    insertStepsAndNext = (input = [], callback) => {
        this.insertSteps(input, () => this.nextIndex(callback));
    }

    render() {
        const { steps } = this.state;
        const { manualControl } = this.props;

        return (
            <View style={layout.screen}>
                <SafeAreaView
                    forceInset={safeAreaInsets.alwaysVertical}
                    style={layout.layoutGrow}
                >
                    <HeaderBar />
                    <View>
                        <StepsList
                            data={steps}
                            step={this.state.index}
                            dashed
                        />
                    </View>
                    <StepsList
                        data={steps}
                        step={this.state.index}
                        containerStyle={null}
                        stepWidth={device.width}
                        RenderItem={StepsDisplay}
                    />

                    {manualControl && (
                        <View style={styles.manuaControl}>
                            <Button title="сбросить" onPress={this.resetState} />
                            <Button title="назад" onPress={this.prevIndex} />
                            <Button title="вперёд" onPress={this.nextIndex} />
                            <Button
                                title="+ добавить"
                                onPress={() => this.insertStepsAndNext([StepExample()])}
                            />
                        </View>
                    )}
                </SafeAreaView>
            </View>
        );
    }
}

StepsWizzard.propTypes = {
    /** Массив конфигураций шагов */
    steps: PropTypes.arrayOf(
        PropTypes.shape(StepModelProps.propTypes),
    ).isRequired,
    /** Показать ручное управление */
    manualControl: PropTypes.bool,
};

StepsWizzard.defaultProps = {
    manualControl: true,
};

export default StepsWizzard;
