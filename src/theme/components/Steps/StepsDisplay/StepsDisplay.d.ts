import * as React from 'react';

import { StepModelInterface } from '../models/StepModelInterface';

export interface StepsDisplayProps extends StepModelInterface {
}

/**
 * Контейнер на всю ширину для отображения подробностней шага
 */
export default class StepsDisplay extends React.PureComponent<StepsDisplayProps, any> {
    render(): JSX.Element;
}
