import React, { PureComponent } from 'react';

import { View, StyleSheet } from 'react-native';

import { FadeInOut } from '~/theme/components/FadeInOut';
import { TextMessage } from '~/theme/components/TextMessage';

import { StepModelProps } from '~/theme/components/Steps/models';

import { device } from '~/theme/vars';

const styles = StyleSheet.create({
    root: {
        width: device.width,
    },
    container: {
        width: device.width,
        flexGrow: 1,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
        // backgroundColor: 'rgba(255, 0, 0, 0.1)',
    },
});

/**
 * Контейнер на всю ширину для отображения подробностней шага
 */
class StepsDisplay extends PureComponent {
    render() {
        const { isActive, children, description } = this.props;

        return (
            <FadeInOut visible={isActive} style={styles.root} delay={400}>
                <View style={styles.container}>
                    {description && <TextMessage text={description} />}
                    {children}
                </View>
            </FadeInOut>
        );
    }
}

StepsDisplay.propTypes = StepModelProps.propTypes;
StepsDisplay.defaultProps = StepModelProps.defaultProps;

export default StepsDisplay;
