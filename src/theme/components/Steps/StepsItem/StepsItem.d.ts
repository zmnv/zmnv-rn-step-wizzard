import * as React from 'react';

import { StepModelInterface } from '../models/StepModelInterface';

export interface StepsItemProps extends StepModelInterface {
}

/**
 * Анимированный компонент для отображения «шага»
 */
export default class StepsItem extends React.PureComponent<StepsItemProps, any> {
    render(): JSX.Element;
}
