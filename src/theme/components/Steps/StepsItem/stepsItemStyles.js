import { StyleSheet } from 'react-native';
import { material } from 'react-native-typography';

import { colors, units } from '~/theme/vars';

export const iconBackgroundSize = units.u68;
export const iconGroupSize = iconBackgroundSize + units.u24;

export const stepsItemStyles = StyleSheet.create({
    root: {
        width: 100,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginRight: units.u44,
        paddingBottom: units.u80,
        position: 'relative',
    },
    iconGroupContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: units.u28,
        width: iconGroupSize,
        height: iconGroupSize,
        overflow: 'hidden',
        position: 'relative',
    },
    iconBackgroundSuccess: {
        backgroundColor: colors.types.success,
    },
    iconSuccessColor: {
        color: colors.types.success,
    },
    iconSuccess: {
        backgroundColor: colors.types.successOpacity,
        padding: units.u8,
        borderRadius: units.u16,
        overflow: 'hidden',
    },
    iconBackground: {
        ...StyleSheet.absoluteFillObject,
        width: iconBackgroundSize,
        height: iconBackgroundSize,
        left: 'auto',
        right: 'auto',
        top: 'auto',
        bottom: 'auto',
        backgroundColor: colors.base.gray,
        zIndex: 1,
    },
    iconProtector: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: colors.base.black,
        // backgroundColor: 'red',
        borderRadius: units.u24,
    },
    titleContainer: {
        ...StyleSheet.absoluteFillObject,
        top: 0,
        bottom: 'auto',
        // backgroundColor: 'red',
    },
    title: {
        ...material.titleWhiteObject,
        lineHeight: units.u24,
        textAlign: 'center',
    },
    titleSmall: {
        ...material.body1WhiteObject,
        color: colors.base.gray,
        lineHeight: units.u20,
        textAlign: 'center',
    },
});
