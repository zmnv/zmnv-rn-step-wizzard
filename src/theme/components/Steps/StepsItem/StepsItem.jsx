import React, { PureComponent } from 'react';
import { View, Animated, Easing } from 'react-native';

import { Icon } from '~/theme/components/Icon';
import { StepModelProps } from '~/theme/components/Steps/models';

import { units } from '~/theme/vars';

import { stepsItemStyles as styles, iconBackgroundSize } from './stepsItemStyles';

/**
 * 8 – размер точки
 * 68 - размер сквиркла
 * */
const CIRCLE_SIZE = 8 / iconBackgroundSize;

const ANIMATION_CONFIG = {
    duration: 256,
    easing: Easing.cubic,
};

/**
 * Анимированный компонент для отображения «шага»
 * 
 * * Контейнер с закруглёнными краями и тусклым фоном,
 *   внутри которого по центру размещена иконка.
 *   (сквиркл <-> точка)
 *
 * * Заголовок (body gray <-> title white)
 */
class StepsItem extends PureComponent {
    state = {
        isActive: this.props.isActive,
    };

    animatedValue = new Animated.Value(this.props.isActive ? 1 : 0);

    iconBackground = {
        ...styles.iconBackground,
        opacity: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [1, this.props.isFinal ? 0.3 : 0.1],
        }),
        borderRadius: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [128, 24],
        }),
        transform: [{
            scale: this.animatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [CIRCLE_SIZE, 1],
            }),
        }],
    }

    iconProtectorStyle = {
        ...styles.iconProtector,
        transform: [{
            scale: this.animatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [CIRCLE_SIZE * 3, 1],
            }),
        }],
    }

    textStyle = {
        color: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [styles.titleSmall.color, styles.title.color],
        }),
        fontSize: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [styles.titleSmall.fontSize, styles.title.fontSize],
        }),
        lineHeight: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [styles.titleSmall.lineHeight, styles.title.lineHeight],
        }),
        textAlign: styles.titleSmall.textAlign,
    }

    textContainerStyle = {
        ...styles.titleContainer,
        top: this.animatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [units.u64, units.u96],
        }),
    }

    componentDidUpdate(prevProps) {
        const { isActive } = this.props;

        if (prevProps.isActive !== isActive) {
            Animated.timing(this.animatedValue, {
                toValue: isActive ? 1 : 0,
                ...ANIMATION_CONFIG,
            }).start(() => {
                this.setState({ isActive });
            });
        }
    }


    rootStyle({isFinal}) {
        if (isFinal) {
            return {
                ...styles.root,
                marginRight: 0,
            };
        }

        return styles.root;
    }

    renderTextView() {
        return (
            <Animated.View style={this.textContainerStyle}>
                <Animated.Text style={this.textStyle} allowFontScaling={false} numberOfLines={2}>
                    {this.props.title}
                </Animated.Text>
            </Animated.View>
        );
    }

    renderIconView() {
        const { isFinal, isActive, iconName } = this.props;

        const iconStyles = {
            container: isFinal ? styles.iconSuccess : null,
            icon: isFinal ? styles.iconSuccessColor : null,
        };

        return (
            <View style={styles.iconGroupContainer}>
                <Animated.View style={{ zIndex: 1, opacity: this.animatedValue }}>
                    {(isActive || this.state.isActive) && (
                        <View style={iconStyles.container}>
                            <Icon name={iconName} style={iconStyles.icon} />
                        </View>
                    )}
                </Animated.View>
                <Animated.View style={[
                    this.iconBackground,
                    isFinal && this.state.isActive
                        && styles.iconBackgroundSuccess,
                ]}
                />
                <Animated.View style={this.iconProtectorStyle} />
            </View>
        );
    }

    render() {
        return (
            <View style={this.rootStyle(this.props)}>
                {this.renderIconView()}
                {this.renderTextView()}
            </View>
        );
    }
}

StepsItem.propTypes = StepModelProps.propTypes;
StepsItem.defaultProps = StepModelProps.defaultProps;

export default StepsItem;
