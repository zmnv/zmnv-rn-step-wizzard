import { StepsList } from './StepsList';
import { StepsItem } from './StepsItem';
import { StepsWizzard } from './StepsWizzard';
import { StepsScroll } from './StepsScroll';
import { StepsDisplay } from './StepsDisplay';

/**
 * Группа компонентов для интерфейса пошаговых действий
 */
export const Steps = {
    List: StepsList,
    Item: StepsItem,
    Wizzard: StepsWizzard,
    Scroll: StepsScroll,
    Display: StepsDisplay,
};

export * from './models';
