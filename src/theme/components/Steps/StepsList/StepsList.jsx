import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { StyleSheet } from 'react-native';
import { device, units } from '~/theme/vars';

import { StepsItem } from '../StepsItem';
import { StepsScroll } from '../StepsScroll';
import { StepModelProps } from '../models/StepModel';

const styles = StyleSheet.create({
    containerStyle: {
        height: 150,
        marginTop: units.u8,
        paddingLeft: device.width / 2 - units.u48,
        paddingRight: device.width / 2 - units.u48,
    },
});

class StepsList extends PureComponent {
    scrollRef = React.createRef();

    componentDidUpdate(prevProps) {
        if (
            prevProps.step !== this.props.step
        ) {
            if (this.props.step === 0) {
                this.resetStep();
                return;
            }

            if (prevProps.step < this.props.step) {
                this.nextStep();
            }

            if (prevProps.step > this.props.step) {
                this.prevStep();
            }
        }
    }

    nextStep() {
        this.scrollRef.current.nextStep();
    }

    prevStep() {
        this.scrollRef.current.prevStep();
    }

    resetStep() {
        this.scrollRef.current.resetStep();
    }

    render() {
        const { step, stepWidth, data, RenderItem, dashed, containerStyle } = this.props;

        return (
            <StepsScroll
                ref={this.scrollRef}
                dashed={dashed}
                containerStyle={containerStyle}
                stepWidth={stepWidth}
            >
                {data.map((props, index) => {
                    const isActive = this.props.renderItemWithActive
                        && step === index;

                    return (
                        <RenderItem
                            key={props.title}
                            isActive={isActive}
                            {...props}
                        />
                    );
                })}
            </StepsScroll>
        );
    }
}

StepsList.propTypes = {
    /** Индекс активного шага */
    step: PropTypes.number,
    /** Конфигурация списока шагов */
    data: PropTypes.arrayOf(
        PropTypes.shape(StepModelProps.propTypes),
    ),
    /** Ширина одного шага */
    stepWidth: PropTypes.number,
    /** Компонент для отображения элемента списка */
    RenderItem: PropTypes.any,
    /** 
     * Передавать `isActive` в компонент `props.RenderItem`.
     * 
     * Поставьте `false`, чтобы игнорировать
     * изменение состояния «активный шаг» при рендере
     * элементов `RenderItem`.
     * 
     * ```jsx
     * // renderItemWithActive = true
     * <RenderItem isActive={step === index} />
     * 
     * // renderItemWithActive = false
     * <RenderItem isActive={false} />
     * ```
     */
    renderItemWithActive: PropTypes.bool,
    /** Показать пунктирную линию */
    dashed: PropTypes.bool,
    /** Стиль корневого компонента `<StepsScroll />` */
    containerStyle: PropTypes.object,
};

StepsList.defaultProps = {
    step: 0,
    data: [
        {
            title: 'Start',
            iconName: 'search',
        },
        {
            title: 'Finish',
            iconName: 'done',
            isFinal: true,
        },
    ],
    stepWidth: 144,
    RenderItem: StepsItem,
    renderItemWithActive: true,
    dashed: false,
    containerStyle: styles.containerStyle,
};

export default StepsList;
