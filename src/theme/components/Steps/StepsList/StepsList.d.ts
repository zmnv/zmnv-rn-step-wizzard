import * as React from 'react';

import { StepModelInterface } from '../models/StepModelInterface';

export interface StepsListProps {
    /**
     * Индекс активного шага
     */
    step?: number;
    /**
     * Конфигурация списока шагов
     */
    data?: StepModelInterface[];
    /**
     * Ширина одного шага
     */
    stepWidth?: number;
    /**
     * Компонент для отображения элемента списка
     */
    RenderItem?: any;
    /**
     * Передавать `isActive` в компонент `props.RenderItem`.
     * Поставьте `false`, чтобы игнорировать
     * изменение состояния «активный шаг» при рендере
     * элементов `RenderItem`.
     * ```jsx
     * // renderItemWithActive = true
     * <RenderItem isActive={step === index} />
     * // renderItemWithActive = false
     * <RenderItem isActive={false} />
     * ```
     */
    renderItemWithActive?: boolean;
    /**
     * Показать пунктирную линию
     */
    dashed?: boolean;
    /**
     * Стиль корневого компонента `<StepsScroll />`
     */
    containerStyle?: StyleSheetList;
}

export default class StepsList extends React.PureComponent<StepsListProps, any> {
    render(): JSX.Element;

}
