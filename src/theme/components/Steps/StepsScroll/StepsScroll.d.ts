import * as React from 'react';

export interface StepsScrollProps {
    /**
     * Контент внутри скролла
     */
    children: React.ReactNode;
    /**
     * Ширина одного шага
     */
    stepWidth?: number;
    /**
     * Показать/скрыть пунктирную линию на фоне
     */
    dashed?: boolean;
    /**
     * Стиль контейнера пунктирной линии на фоне
     */
    dashStyle?: StyleSheetList;
    /**
     * Стиль корневого контейнера `<ScrollView />`
     */
    containerStyle?: StyleSheetList;
}

/**
 * Компонент пошагового скролла
 */
export default class StepsScroll extends React.PureComponent<StepsScrollProps, any> {
    render(): JSX.Element;
}
