import React from 'react';
import PropTypes from 'prop-types';

import { Dash } from '~/theme/components/Dash';

/** 
 * Элемент пунктирной линии для пошагового волшебника 
 */
const DashView = props => {
    // eslint-disable-next-line react/prop-types
    const { dashed, style } = props;

    if (dashed) {
        return <Dash style={style} />;
    }

    return null;
};

DashView.propTypes = {
    /**
     * Показать/скрыть элемент
     */
    dashed: PropTypes.bool,
    /** 
     * Стиль контейнера с точками
     */
    style: PropTypes.object,
};

DashView.defaultProps = {
    dashed: true,
    style: null,
};

export default DashView;
