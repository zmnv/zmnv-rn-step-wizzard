import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View, ScrollView, StyleSheet } from 'react-native';

import { DashView } from './components';

const styles = StyleSheet.create({
    containerStyle: {},
    dashContainer: {
        position: 'absolute',
        top: 45,
        left: 34,
        right: 30,
    },
});

/**
 * Компонент пошагового скролла
 */
class StepsScroll extends PureComponent {
    state = {
        scrollPosition: 0,
    }

    scrollRef = React.createRef();

    /** 
     * TODO: next, prev и reset можно заменить на 1 метод `goToStep(step)`
     * и на 1 проп `currentStep` и рассчитывать позицию по индексу шага,
     * но я не буду.
     */

    scrollToStatePosition = () => {
        requestAnimationFrame(() => {
            this.scrollRef.current.scrollTo({
                x: this.state.scrollPosition,
                animated: true,
            });
        });
    }

    nextStep() {
        this.setState(({scrollPosition}) => ({
            scrollPosition: scrollPosition + this.props.stepWidth,
        }), this.scrollToStatePosition);
    }

    prevStep() {
        this.setState(({scrollPosition}) => ({
            scrollPosition: scrollPosition - this.props.stepWidth,
        }), this.scrollToStatePosition);
    }

    resetStep() {
        /** 
         * 1. Ждём, пока шаг будет удалён из списка на уровне выше
         * 2. Скроллим в начало
         */

        this.setState({
            scrollPosition: 0,
        }, this.scrollToStatePosition);
    }


    render() {
        const { children, containerStyle, dashStyle, dashed } = this.props;

        return (
            <ScrollView
                ref={this.scrollRef}
                bounces={false}
                horizontal
                contentContainerStyle={containerStyle}
                removeClippedSubviews
                scrollEnabled={false}
            >
                {/** TODO: Стили */}
                <View style={{flexDirection: 'row'}}>
                    <DashView dashed={dashed} style={dashStyle} />
                    {children}
                </View>
            </ScrollView>
        );
    }
}

StepsScroll.propTypes = {
    /** 
     * Контент внутри скролла
     */
    children: PropTypes.node.isRequired,
    /**
     * Ширина одного шага
     */
    stepWidth: PropTypes.number,
    /**
     * Показать/скрыть пунктирную линию на фоне
     */
    dashed: PropTypes.bool,
    /**
     * Стиль контейнера пунктирной линии на фоне
     */
    dashStyle: PropTypes.object,
    /** 
     * Стиль корневого контейнера `<ScrollView />`
     */
    containerStyle: PropTypes.object,
};

StepsScroll.defaultProps = {
    stepWidth: 144,
    dashed: false,
    dashStyle: styles.dashContainer,
    containerStyle: styles.containerStyle,
};

export default StepsScroll;
