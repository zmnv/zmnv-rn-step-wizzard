import * as React from 'react';

export interface TextMessageProps {
    /**
     * Текст сообщения
     */
    text: string | React.ReactNode;
}

/**
 * Контейнер с текстом по центру
 */
export default class TextMessage extends React.PureComponent<TextMessageProps, any> {
    render(): React.Element;
}
