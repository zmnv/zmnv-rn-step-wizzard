import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { View, Text, StyleSheet } from 'react-native';
import { material } from 'react-native-typography';

import { units, device } from '~/theme/vars';
import { textColors } from '~/theme/styles';

/** TODO: унести */
const verticalPaddings = value => {
    const devices = {
        iphone5s: 568,
        iphone6s: 667,
        iphone6sPlus: 736,
        iphoneX: 812,
    };

    if (value >= devices.iphone5s && value < devices.iphone6s) {
        return units.u48 * 2 + units.u20;
    }

    if (value >= devices.iphone6s && value < devices.iphone6sPlus) {
        return units.u80 * 2 + units.u16;
    }

    if (value >= devices.iphone6sPlus && value < devices.iphoneX) {
        return units.u96 * 2 + units.u24;
    }

    if (value >= devices.iphoneX) {
        return units.u128 * 2;
    }

    return units.u24;
};

const styles = StyleSheet.create({
    root: {
        height: verticalPaddings(device.height),
        justifyContent: 'center',
        // backgroundColor: 'rgba(0, 255, 0, 0.1)',
    },
    text: {
        maxWidth: 300,
        alignSelf: 'center',
        textAlign: 'center',
        ...material.body1WhiteObject,
        ...textColors.textSecondaryColor,
    },
});

/**
 * Контейнер с текстом по центру
 */
class TextMessage extends PureComponent {
    render() {
        const { text } = this.props;

        return (
            <View style={styles.root}>
                <Text style={styles.text}>{text}</Text>
            </View>
        );
    }
}

TextMessage.propTypes = {
    /** Текст сообщения */
    text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.node,
    ]).isRequired,
};

export default TextMessage;
