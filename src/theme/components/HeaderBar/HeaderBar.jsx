import React, { PureComponent } from 'react';

import { View, StatusBar } from 'react-native';
import { IconTouchable } from '~/theme/components/Icon';

import { layout, iconColors } from '~/theme/styles';

/** Простая версия шапки с иконкой «закрыть» */
class HeaderBar extends PureComponent {
    render() {
        return (
            <View>
                <StatusBar barStyle="light-content" />
                <View style={layout.paddingHorizontalSmall}>
                    <IconTouchable
                        name="close"
                        iconStyle={iconColors.iconPrimaryColor}
                        onPress={() => {}}
                    />
                </View>
            </View>
        );
    }
}

export default HeaderBar;
