import React from 'react';
import PropTypes from 'prop-types';
import DashModule from 'react-native-dash';

import { StyleSheet } from 'react-native';
import { colors } from '../../vars/colors';
import { units } from '~/theme/vars';

const styles = StyleSheet.create({
    dashItem: {
        borderRadius: units.u8,
        backgroundColor: colors.base.grayInactive,
    },
});

const Dash = props => {
    const { style, dashStyle, ...otherProps } = props;
    return (
        <DashModule
            dashGap={5}
            dashLength={1}
            dashThickness={1}
            {...otherProps}
            style={style}
            dashStyle={dashStyle}
        />
    );
};

Dash.propTypes = {
    style: PropTypes.object,
    dashStyle: PropTypes.object,
};

Dash.defaultProps = {
    style: null,
    dashStyle: styles.dashItem,
};

export default Dash;
