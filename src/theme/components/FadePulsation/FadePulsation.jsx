import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Animated } from 'react-native';

/**
 * Контейнер анимации:
 * - цикличная «пульсация» прозрачности
 */
class FadePulsation extends PureComponent {
    fadeAnim = new Animated.Value(this.props.fromValue);

    rootStyle = {
        opacity: this.fadeAnim,
    };

    componentDidMount() {
        requestAnimationFrame(this.showChildren);
    }

    showChildren = () => {
        Animated.timing(
            this.fadeAnim,
            {
                toValue: this.props.toValue,
                duration: this.props.duration,
                delay: this.props.delay,
                // useNativeDriver: true,
            },
        ).start(this.hideChildren);
    }

    hideChildren = () => {
        Animated.timing(
            this.fadeAnim,
            {
                toValue: this.props.fromValue,
                duration: this.props.duration,
                delay: this.props.delay,
            },
        ).start(this.showChildren);
    }

    render() {
        const { children } = this.props;

        return (
            <Animated.View style={this.rootStyle}>
                {children}
            </Animated.View>
        );
    }
}

FadePulsation.propTypes = {
    children: PropTypes.any.isRequired,
    toValue: PropTypes.number,
    fromValue: PropTypes.number,
    duration: PropTypes.number,
    delay: PropTypes.number,
};

FadePulsation.defaultProps = {
    toValue: 1,
    fromValue: 0,
    duration: 500,
    delay: 0,
};

export default FadePulsation;
