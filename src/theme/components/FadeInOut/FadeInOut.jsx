import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Animated, Easing, InteractionManager } from 'react-native';

/**
 * Контейнер анимации:
 * - появление / исчезновение блока
 * в зависимости от `props.visible`
 * 
 * - изменение пропа `visible` снаружи вызовет обновление состояния
 */
class FadeInOut extends PureComponent {
    state = {
        isVisible: this.props.visible,
    };

    visibility = new Animated.Value(this.props.visible ? 1 : 0);

    componentDidUpdate(prevProps) {
        if (prevProps.visible !== this.props.visible) {
            const { visible, delay, duration } = this.props;
            const fadeInOutHandle = InteractionManager.createInteractionHandle();

            Animated.timing(this.visibility, {
                toValue: visible ? 1 : 0,
                duration,
                delay,
                easing: Easing.ease,
                useNativeDriver: true,
            }).start(() => {
                this.setState({ isVisible: visible });
                InteractionManager.clearInteractionHandle(fadeInOutHandle);
            });
        }
    }

    render() {
        const { style, children, visible, ...otherProps } = this.props;
        const { isVisible } = this.state;

        const rootStyle = {
            ...style,
            ...{
                opacity: this.visibility,
            },
        };

        return (
            <Animated.View
                style={rootStyle}
                {...otherProps}
            >
                {visible || isVisible ? children : null}
            </Animated.View>
        );
    }
}

FadeInOut.propTypes = {
    /**
     * Компонент ребёнка, монтируется только при `props.visible === true`
     */
    children: PropTypes.node.isRequired,
    /**
     * Показать/скрыть
     */
    visible: PropTypes.bool.isRequired,
    /** 
     * Задержка начала воспроизведения анимации (мс)
     */
    delay: PropTypes.number,
    /** 
     * Длительность анимации (мс)
     */
    duration: PropTypes.number,
    /** 
     * Стиль главного контейнера
     */
    style: PropTypes.any,
};

FadeInOut.defaultProps = {
    delay: 0,
    duration: 240,
    style: {},
};

export default FadeInOut;
