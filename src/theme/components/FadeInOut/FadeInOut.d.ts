import * as React from 'react';

export interface FadeInOutProps {
    /**
     * Компонент ребёнка, монтируется только при `props.visible === true`
     */
    children: React.ReactNode;
    /**
     * Показать/скрыть
     */
    visible: boolean;
    /**
     * Задержка начала воспроизведения анимации (мс)
     */
    delay?: number;
    /**
     * Длительность анимации (мс)
     */
    duration?: number;
    /**
     * Стиль главного контейнера
     */
    style?: any;
}

/**
 * Контейнер анимации:
 * - появление / исчезновение блока
 * в зависимости от `props.visible`
 * 
 * - изменение пропа `visible` снаружи вызовет обновление состояния
 */
export default class FadeInOut extends React.PureComponent<FadeInOutProps, any> {
    render(): React.Element;
}
