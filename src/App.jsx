import React, { PureComponent } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar } from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';
import { material } from 'react-native-typography';

import { safeAreaInsets, colors } from '~/theme/vars';
import { layout } from '~/theme/styles';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n'
        + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n'
        + 'Shake or press menu button for dev menu',
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    welcome: {
        ...material.titleWhite,
        ...layout.marginBottomNormal,
        textAlign: 'center',
    },
    instructions: {
        ...material.captionWhite,
        textAlign: 'center',
        ...layout.marginBottomBase,
    },
});

class App extends PureComponent {
    render() {
        return (
            <View style={layout.screen}>
                <StatusBar backgroundColor={colors.surfaces.primary} barStyle="light-content" />
                <SafeAreaView forceInset={safeAreaInsets.alwaysVertical} style={{flex: 1}}>
                    <View style={styles.container}>
                        <Text style={styles.welcome}>Welcome to React Native!</Text>
                        <Text style={styles.instructions}>To get started, edit App.js</Text>
                        <Text style={styles.instructions}>{instructions}</Text>
                    </View>
                </SafeAreaView>
            </View>
        );
    }
}

export default App;
